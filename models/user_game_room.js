'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_game_room.init({
    number_room: DataTypes.INTEGER,
    user_game_id1: DataTypes.INTEGER,
    user_game_id2: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_room',
  });
  return user_game_room;
};